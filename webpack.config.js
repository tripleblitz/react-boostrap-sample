module.exports = {
    entry: './src/client/app.jsx',
    output: {
        path: __dirname,
        filename: './public/bundle.js'
    },
    resolve: {
        modules: ['node_modules', './src/common/components', './src/common/libs'],
        extensions: ['.js', '.jsx']
    },
    module: {
        loaders: [
            {
                loader: 'babel-loader',
                query: { presets: ['react', 'es2015'] },
                test: /\.jsx?$/,
                exclude: /(node_modules)/
            }
        ]
    }
};
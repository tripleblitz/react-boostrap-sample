import React from 'react';
import ReactDOM from 'react-dom';

import {Grid, Row, Col, Button} from 'react-bootstrap';

const App = (
    <Grid fluid={true}>
        <Row>
            <Col xsHidden sm={4}>
                <div style={{backgroundColor: "red"}}>ABC</div>
            </Col>
            <Col xs={12} sm={4}>
                <div style={{backgroundColor: "yellow"}}>DEF</div>
            </Col>
            <Col xs={12} sm={4}>
                <Button>Default</Button>
            </Col>
        </Row>
    </Grid>
);

ReactDOM.render(
    App,
    document.getElementById('root'));
require('dotenv').config();

var express = require('express');

const app = express();

const listenIP = process.env.listenIP;
const listenPort = process.env.listenPort; 

app.use(express.static('public'));
app.listen(listenPort, listenIP, function () {
  console.log('Web server listening on ' + listenIP + ':' + listenPort);
});

